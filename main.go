// Copyright 2014 Adam Jimerson. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
)

const version = "0.0.1"

var (
	debug = false
	level string
)

func usage() {
	cmd := flag.Arg(0)
	fmt.Fprintf(os.Stderr,
		`Usage: %s [options] command

Verson: %s

Options:
  --debug  Display debug output

Commands:
  off    Turns off screen blanking
  on     Turns on screen blanking`, cmd, version)
}

func init() {
	flag.Usage = usage
	flag.BoolVar(&debug, "debug", false, "Display debug output")
	flag.Parse()
}

func logHandler(lvl, msg string) {
	switch lvl {
	case "DEBUG":
		if debug {
			log.Printf("[%s]: %s\n", lvl, msg)
		}
	case "INFO":
		if debug {
			log.Printf("[%s]: %s\n", lvl, msg)
		}
	case "WARN":
		log.Printf("[%s]: %s\n", lvl, msg)
	case "ERROR":
		log.Fatalf("[%s]: %s\n", lvl, msg)
	default:
		log.Printf("[%s]: %s\n", lvl, msg)
	}
}

func main() {
	if flag.NArg() == 0 {
		logHandler("WARN", "a command is required")
		usage()
		os.Exit(2)
	}

	switch flag.Arg(0) {
	case "off":
		logHandler("DEBUG", fmt.Sprintf("command \"%s\" given", flag.Arg(0)))
		err := displayOn()
		if err != nil {
			logHandler("ERROR", err.Error())
		}
	case "on":
		logHandler("DEBUG", fmt.Sprintf("command \"%s\" given", flag.Arg(0)))
		err := displayOff()
		if err != nil {
			logHandler("ERROR", err.Error())
		}
	default:
		logHandler("ERROR", fmt.Sprintf("unknown command: %s\n", flag.Arg(0)))
	}
}

func displayOn() error {
	// turn off Display Power Management Signaling
	cmd := exec.Command("xset", "-dpms")
	err := cmd.Start()
	if err != nil {
		return err
	}

	// turn off screen blanking
	cmd = exec.Command("xset", "s", "off")
	err = cmd.Start()
	if err != nil {
		return err
	}

	return nil
}

func displayOff() error {
	// turn on Display Power Management Signaling
	cmd := exec.Command("xset", "dpms")
	err := cmd.Start()
	if err != nil {
		return err
	}

	// turn on screen blanking
	// default one hour
	t := 3600
	cmd = exec.Command("xset", "s", string(t), string(t))
	err = cmd.Start()
	if err != nil {
		return err
	}

	return nil
}
